<?php

namespace ReefExtra\RandomSubmission;

use \Reef\Submission\Submission;
use \Reef\Components\FieldValue;
use \joshtronic\LoremIpsum;

/**
 * Utility class for generating random submissions
 */
class RandomSubmission {
	
	/**
	 * The lorem ipsum generator
	 * @type LoremIpsum
	 */
	private static $lorem = null;
	
	/**
	 * Get a lorem ipsum generator
	 * @return LoremIpsum The generator
	 */
	public static function getLorem() {
		if(self::$lorem === null) {
			self::$lorem = new LoremIpsum();
			self::$lorem->sentence(); // Remove first standard sentence
		}
		return self::$lorem;
	}
	
	/**
	 * Fill a submission with random data
	 * @param Submission $Submission The submission to fill
	 */
	public static function fillSubmission(Submission $Submission) {
		$Form = $Submission->getForm();
		
		$a_fields = $Form->getValueFields();
		foreach($a_fields as $Field) {
			$a_declaration = $Field->getDeclaration();
			$s_name = $a_declaration['name'];
			$FieldValue = $Submission->getFieldValue($s_name);
			
			if($FieldValue instanceof \Reef\Components\AbstractSingleChoice\AbstractSingleChoiceValue) {
				$FieldValue->fromStructured(empty($a_declaration['options']) ? null : $a_declaration['options'][array_rand($a_declaration['options'])]['name']);
			}
			else if($FieldValue instanceof \Reef\Components\Upload\UploadValue) {
				$FieldValue->fromStructured([]);
			}
			else {
				static::genericRandomFieldValue($FieldValue);
			}
		}
	}
	
	/**
	 * Assign a generic random value to a fieldValue
	 * @param FieldValue $FieldValue The field value
	 */
	protected static function genericRandomFieldValue(FieldValue $FieldValue) {
		$a_flatStructure = $FieldValue->getField()->getFlatStructure();
		
		$a_flatField = [];
		
		foreach($a_flatStructure as $m_dataFieldName => $a_dataFieldStructure) {
			$m_dataFieldValue = null;
			
			switch($a_dataFieldStructure['type']) {
				case \Reef\Storage\Storage::TYPE_TEXT:
					$m_dataFieldValue = static::randText($a_dataFieldStructure);
				break;
				
				case \Reef\Storage\Storage::TYPE_BOOLEAN:
					$m_dataFieldValue = static::randBool($a_dataFieldStructure);
				break;
				
				case \Reef\Storage\Storage::TYPE_INTEGER:
					$m_dataFieldValue = static::randInt($a_dataFieldStructure);
				break;
				
				case \Reef\Storage\Storage::TYPE_FLOAT:
					$m_dataFieldValue = static::randFloat($a_dataFieldStructure);
				break;
			}
			
			$a_flatField[$m_dataFieldName] = $m_dataFieldValue;
		}
		
		$FieldValue->fromFlat($a_flatField);
	}
	
	/**
	 * Return random text
	 * @param array The structure array
	 * @return string The text
	 */
	protected static function randText(array $a_structure) : string {
		$i_limit = max((int)($a_structure['limit']??1000), 1);
		
		if($i_limit < 25) {
			$s_text = static::getLorem()->word();
		}
		else if($i_limit < 100) {
			$s_text = static::getLorem()->words(5);
		}
		else {
			$s_text = static::getLorem()->sentence();
		}
		
		return substr($s_text, 0, $i_limit);
	}
	
	/**
	 * Return random boolean
	 * @param array The structure array
	 * @return bool The boolean
	 */
	protected static function randBool(array $a_structure) : bool {
		return (bool)rand(0, 1);
	}
	
	/**
	 * Return random integer
	 * @param array The structure array
	 * @return int The integer
	 */
	protected static function randInt(array $a_structure) : int {
		$i_min = isset($a_structure['min']) ? (int)$a_structure['min'] : null;
		$i_max = isset($a_structure['max']) ? (int)$a_structure['max'] : null;
		
		if($i_min === null && $i_max === null) {
			$i_min = 0;
			$i_max = 10000;
		}
		else if($i_min === null) {
			$i_min = ($i_max > 0) ? -$i_max : $i_max - 10;
		}
		else if($i_max === null) {
			$i_max = ($i_min < 0) ? -$i_min : $i_max + 10;
		}
		
		return rand($i_min, $i_max);
	}
	
	/**
	 * Return random float
	 * @param array The structure array
	 * @return float The float
	 */
	protected static function randFloat(array $a_structure) : float {
		return round(10 * (rand() / getrandmax()), 5);
	}
	
}
