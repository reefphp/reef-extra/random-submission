<?php

namespace ReefExtra\RandomSubmissionTests;

use PHPUnit\Framework\TestCase;
use \ReefExtra\RandomSubmission\RandomSubmission;

final class RandomSubmissionTest extends TestCase {
	
	private static $Reef;
	
	public static function setUpBeforeClass() {
		
		// Specify which components we want to use
		$Setup = new \Reef\ReefSetup(
			new \Reef\Storage\NoStorageFactory(),
			new \Reef\Layout\bootstrap4\bootstrap4(),
			new \Reef\Session\TmpSession()
		);
		$Setup->addComponent(new \Reef\Components\Upload\UploadComponent);
		
		static::$Reef = new \Reef\Reef(
			$Setup,
			[
				'files_dir' => '/tmp/',
			]
		);
	}
	
	/**
	 */
	public function testCreatesValidRandomSubmission(): void {
		$Form = static::$Reef->getTempFormFactory()->createFromFile(__DIR__ . '/definition.yml');
		
		$Submission = $Form->newSubmission();
		$Submission->emptySubmission();
		
		RandomSubmission::fillSubmission($Submission);
		
		$b_valid = $Submission->validate();
		if(!$b_valid) {
			var_dump($Submission->toStructured(), $Submission->getErrors());
		}
		
		$this->assertTrue($b_valid);
	}
}
